#include<stdio.h>
  void will_this_change_a(int b)
{
  b = 50;
  printf("The Value Of a In Called Function =%d\n",b);
  return;
}
  int main()
{
  int a = 100;
  will_this_change_a(a);
  printf("The Value Of a In Calling Function =%d\n",a);
  return 0;
}
  
  
/* If we change the parameter in the called function it wont change in the calling function */
  
  