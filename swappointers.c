#include<stdio.h>
int input()
{
 int a;
 printf("Enter the number\n");
 scanf("%d",&a);
 return a;
}
void compute(int *a,int *b)
{
 int temp=*a;
 *a=*b;
 *b=temp;
}
void output(int a,int b)
{
 printf("\nThe interchanged numbers are a=%d\nb=%d\n",a,b);
}
int main()
{
 printf("Enter a\n");
 int a=input();
 printf("Enter b\n");
 int b=input();
 printf("\nThe numbers before interchange are a=%d\nb=%d",a,b);
 compute(&a,&b);
 output(a,b);
 return 0;
}
 